<?php

/**
 * Correios
 *
 * Correios Shipping Method for Magento 2.
 *
 * @package ImaginationMedia\Correios
 * @author Igor Ludgero Miura <igor@imaginemage.com>
 * @copyright Copyright (c) 2017 Imagination Media (http://imaginemage.com/)
 * @license https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

namespace ImaginationMedia\Correios\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use ImaginationMedia\Correios\Helper\Data as Helper;

class PostingMethods implements ArrayInterface
{
    /**
     * @var \ImaginationMedia\Correios\Helper\Data
     */
    private $_helper;

    public function __construct(
        Helper $helper
    )
    {
        $this->_helper = $helper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $methods = $this->_helper->getMethodsData();

        $return = [];
        foreach ($methods as $method) {
            $return[] = ['value' => $method['code'], 'label' => $method['name'] . ' (' . $method['code'] . ')'];
        }

        return $return;
    }
}
